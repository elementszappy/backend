var http = require('http');
var express = require('express');
var mongoose = require('./mongodb/mongodb.service');
var app = express();
var slack = require('./slack/slack.service');

app.set('port', 8000);
app.use(function(request, response, next) {
    response.header("Access-Control-Allow-Origin", "*");
    response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

mongoose.connect();

app.get('/tweets', function(req, res) {
    mongoose.findAllTweets(res);
});
http.createServer(app).listen(app.get('port'), function() {
    console.log('Express server listening on port ' + app.get('port'));
    slack.start();
});

module.exports = app;