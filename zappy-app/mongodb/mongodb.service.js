var mongoose = require('mongoose');
mongoose.model('tweets', {
    text: String,
    created_at: String
});
module.exports = {
    connect: function() {
        mongoose.connect('mongodb://localhost:27017/zappy');
    },
    saveTweetsIntoDataBase: function(tweets) {
        mongoose.model('tweets').insertMany(tweets);
    },
    deleteOldTweetsIntoDataBase: function() {
        mongoose.connection.collections['tweets'].drop(function(err) {
            console.log('collection dropped');
        });
    },
    findAllTweets: function(res) {
        mongoose.model('tweets').find(function(err, tweets) {
            res.json(tweets);
        });
    }
}